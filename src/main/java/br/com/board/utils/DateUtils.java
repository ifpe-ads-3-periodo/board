package br.com.board.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author Ronyeri Marinho
 */

public class DateUtils {

	public static LocalDate dateToLocalDate(Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
}
