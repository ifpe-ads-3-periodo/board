package br.com.board.utils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ronyeri Marinho
 */

public class StringUtils {

	public static List<String> stringToList(String item) {
		List<String> points = Arrays.asList(item.split(","));
		return points.stream().map(point -> point.trim()).collect(Collectors.toList());
	}

	public static String listToString(List<String> items) {
		return items.stream().map(label -> label).collect(Collectors.joining(","));
	}
}
