package br.com.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.board.model.Milestone;

/**
 * @author Ronyeri Marinho
 */

@Repository
public interface MilestoneDAO extends JpaRepository<Milestone, Long> {

	public Milestone findByBoardIdentifier(String boardIdentifier);

	@Query("SELECT milestone FROM Milestone milestone WHERE milestone.closed = false")
	public List<Milestone> listMilestones();
}
