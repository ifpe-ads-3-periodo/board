package br.com.board.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.board.model.Organization;
import br.com.board.model.User;

/**
 * @author Ronyeri Marinho
 */

@Repository
public interface OrganizationDAO extends JpaRepository<Organization, Long> {

	public Organization findByBoardIdentifierAndDeletedFalse(String boardIdentifier);

	public List<Organization> findByUserAndDeletedFalse(User user, Sort sort);
}
