package br.com.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.board.model.UserGroup;

/**
 * @author Ronyeri Marinho
 */

@Repository
public interface UserGroupDAO extends JpaRepository<UserGroup, Long> {
	@Query("SELECT userGroup FROM UserGroup userGroup WHERE userGroup.boardIdentifier = :boardIdentifier AND userGroup.deleted = false")
	public UserGroup findByBoardIdentifier(@Param("boardIdentifier") String boardIdentifier);

	@Query("SELECT userGroup FROM UserGroup userGroup WHERE userGroup.deleted = false")
	public List<UserGroup> list();
}
