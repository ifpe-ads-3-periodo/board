package br.com.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.board.model.CardSet;

@Repository
public interface CardSetDAO extends JpaRepository<CardSet, Long> {

	public CardSet findByBoardIdentifier(String boardIdentifier);
}
