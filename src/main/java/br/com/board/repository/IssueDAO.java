package br.com.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.board.model.Issue;

/**
 * @author Ronyeri Marinho
 */

@Repository
public interface IssueDAO extends JpaRepository<Issue, Long> {

	@Query("SELECT issue FROM Issue issue WHERE issue.milestone.boardIdentifier = :milestoneId")
	public List<Issue> listByMilestone(@Param("milestoneId") String milestoneId);

	@Query("SELECT issue FROM Issue issue WHERE issue.milestone.boardIdentifier = :milestoneId AND issue.voted = false AND issue.milestone.user.boardIdentifier = :userId")
	public List<Issue> listByMilestoneToVoting(@Param("milestoneId") String milestoneId,
			@Param("userId") String userId);

	public Issue findByBoardIdentifier(String boardIdentifier);
}
