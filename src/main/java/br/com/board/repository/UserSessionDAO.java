package br.com.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.board.model.UserSession;

/**
 * @author Ronyeri Marinho
 */

@Repository
public interface UserSessionDAO extends JpaRepository<UserSession, Integer> {

	@Query("SELECT session FROM UserSession session WHERE deleted = false")
	public List<UserSession> listActiveSessions();

	@Query("SELECT session FROM UserSession session WHERE session.user.boardIdentifier = :boardIdentifier AND session.deleted = false")
	public UserSession findByUserUUID(@Param("boardIdentifier") String userUUID);

	@Query("SELECT session FROM UserSession session WHERE session.token = :token AND session.deleted = false")
	public UserSession findByToken(@Param("token") String token);

	@Modifying
	@Query("UPDATE UserSession SET deleted = true WHERE token = :token")
	public void delete(@Param("token") String token);

}
