package br.com.board.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.board.model.User;

/**
 * @author Ronyeri Marinho
 */

@Repository
public interface UserDAO extends JpaRepository<User, Long> {

	public User findByEmail(String email);

	public User findByBoardIdentifier(String boardIdentifier);
}
