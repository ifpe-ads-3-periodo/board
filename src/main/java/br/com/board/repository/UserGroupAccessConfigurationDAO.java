package br.com.board.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.board.model.UserGroupAccessConfiguration;

/**
 * @author Ronyeri Marinho
 */

@Repository
public interface UserGroupAccessConfigurationDAO extends JpaRepository<UserGroupAccessConfiguration, Long> {

	@Query("SELECT accessConfiguration FROM UserGroupAccessConfiguration accessConfiguration WHERE accessConfiguration.userGroup.boardIdentifier = :userGroupUUID AND accessConfiguration.deleted = false")
	public List<UserGroupAccessConfiguration> listActiveTokensByUserGroup(@Param("userGroupUUID") String userGroupUUID);

	@Query("SELECT accessConfiguration FROM UserGroupAccessConfiguration accessConfiguration WHERE accessConfiguration.deleted = false")
	public List<UserGroupAccessConfiguration> listActiveTokens();

	@Query("SELECT accessConfiguration FROM UserGroupAccessConfiguration accessConfiguration WHERE accessConfiguration.token = :token AND accessConfiguration.userGroup.boardIdentifier = :userGroupUUID AND accessConfiguration.deleted = false")
	public UserGroupAccessConfiguration findByTokenAndUserGroup(@Param("userGroupUUID") String userGroupUUID,
			@Param("token") String token);
}
