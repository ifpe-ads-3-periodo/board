package br.com.board.model;

import java.security.NoSuchAlgorithmException;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.board.model.transport.UserDTO;
import br.com.board.utils.CryptographyUtils;

/**
 * @author Ronyeri Marinho
 */

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "boardIdentifier", unique = true, nullable = false)
	private String boardIdentifier;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@Column(nullable = false)
	private String username;

	@Column(unique = true, nullable = false)
	private String email;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private String salt;

	@ManyToMany
	@Cascade(CascadeType.ALL)
	@JoinTable(name = "user_usergroups", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "group_id") })
	private Set<UserGroup> groups;

	public User() {

	}

	public User(UserDTO userDTO) throws NoSuchAlgorithmException {
		this.setUsername(userDTO.getUsername());
		this.setEmail(userDTO.getEmail());
		this.setSalt(CryptographyUtils.generateInstance().generatePasswordSalt());
		this.setPassword(
				CryptographyUtils.generateInstance().createPasswordWithSalt(userDTO.getPassword(), this.getSalt()));
		this.setBoardIdentifier(
				userDTO.getBoardIdentifier() != null ? userDTO.getBoardIdentifier() : UUID.randomUUID().toString());
	}

	public UserDTO generateTransportWithoutGroups() {
		UserDTO userDTO = new UserDTO();
		userDTO.setBoardIdentifier(this.getBoardIdentifier());
		userDTO.setEmail(this.getEmail());
		userDTO.setUsername(this.getUsername());
		return userDTO;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Set<UserGroup> getGroups() {
		return groups;
	}

	public void setGroups(Set<UserGroup> groups) {
		this.groups = groups;
	}
}
