package br.com.board.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.board.model.transport.CardSetDTO;

/**
 * @author Ronyeri Marinho
 */

@Entity
public class CardSet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(unique = true, nullable = false)
	private String name;

	@Column(name = "boardIdentifier", unique = true, nullable = false)
	private String boardIdentifier;

	@Column(nullable = false)
	private String deck;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	public CardSet() {

	}

	public CardSet(CardSetDTO cardSetDTO) {
		this.setBoardIdentifier(cardSetDTO.getBoardIdentifier() != null ? cardSetDTO.getBoardIdentifier()
				: UUID.randomUUID().toString());
		this.setName(cardSetDTO.getName());
		this.setDeck(cardSetDTO.getDeck());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
