package br.com.board.model;

import java.util.Objects;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import br.com.board.model.transport.IssueDTO;
import br.com.board.utils.StringUtils;

/**
 * @author Ronyeri Marinho
 */

@Entity
public class Issue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true, nullable = false)
	private Long issueId;

	@Column(unique = true, nullable = false)
	private Long issueIid;

	@Column(nullable = false)
	private String name;

	@Lob
	private String description;

	@Column(name = "boardIdentifier", unique = true, nullable = false)
	private String boardIdentifier;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean voted;

	@Column(nullable = true)
	private String point;

	private String url;

	@Column(nullable = false)
	private Long projectId;

	private String labels;

	@ManyToOne
	@JoinColumn(name = "milestone_id")
	private Milestone milestone;

	public Issue() {

	}

	public Issue(IssueDTO issueDTO) {
		this.setBoardIdentifier(
				issueDTO.getBoardIdentifier() != null ? issueDTO.getBoardIdentifier() : UUID.randomUUID().toString());
		this.setIssueId(issueDTO.getIssueId());
		this.setIssueIid(issueDTO.getIssueIid());
		this.setName(issueDTO.getName());
		this.setDescription(issueDTO.getDescription());
		this.setLabels(StringUtils.listToString(issueDTO.getLabels()));
		this.setUrl(issueDTO.getUrl());
		this.setProjectId(issueDTO.getProjectId());
		this.setVoted(false);
		this.setPoint(issueDTO.getPoint());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public Long getIssueIid() {
		return issueIid;
	}

	public void setIssueIid(Long issueIid) {
		this.issueIid = issueIid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public Boolean getVoted() {
		return voted;
	}

	public void setVoted(Boolean voted) {
		this.voted = voted;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public Milestone getMilestone() {
		return milestone;
	}

	public void setMilestone(Milestone milestone) {
		this.milestone = milestone;
	}

	@Override
	public int hashCode() {
		return Objects.hash(boardIdentifier, issueId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Issue other = (Issue) obj;
		return Objects.equals(boardIdentifier, other.boardIdentifier) && Objects.equals(issueId, other.issueId);
	}

}
