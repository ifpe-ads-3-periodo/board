package br.com.board.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import br.com.board.model.transport.OrganizationDTO;

/**
 * @author Ronyeri Marinho
 */

@Entity
public class Organization {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "boardIdentifier", unique = true, nullable = false)
	private String boardIdentifier;

	@Column(nullable = false)
	private String name;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Organization() {

	}

	public Organization(OrganizationDTO organizationDTO, User user) {
		this.setName(organizationDTO.getName());
		this.setDeleted(false);
		this.setBoardIdentifier(organizationDTO.getBoardIdentifier() != null ? organizationDTO.getBoardIdentifier()
				: UUID.randomUUID().toString());
		this.setUser(user);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
