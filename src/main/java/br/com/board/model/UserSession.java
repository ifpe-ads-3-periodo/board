package br.com.board.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author Ronyeri Marinho
 */

@Entity
public class UserSession {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(nullable = false)
	private String token;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@Column(nullable = false)
	private LocalDateTime loginDate;

	@Column(nullable = true)
	private LocalDateTime logoutDate;

	@Column(nullable = false)
	private LocalDateTime expirationDate;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
	private User user;

	public Integer getId() {
		return id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(LocalDateTime loginDate) {
		this.loginDate = loginDate;
	}

	public LocalDateTime getLogoutDate() {
		return logoutDate;
	}

	public void setLogoutDate(LocalDateTime logoutDate) {
		this.logoutDate = logoutDate;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public static UserSession build() {
		UserSession userSession = new UserSession();
		userSession.setToken(UUID.randomUUID().toString());

		LocalDateTime currentDate = LocalDateTime.now();
		userSession.setLoginDate(currentDate);
		userSession.setExpirationDate(currentDate.plusHours(3));
		userSession.setDeleted(false);
		return userSession;
	}

}
