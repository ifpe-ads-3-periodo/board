package br.com.board.model;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import br.com.board.model.transport.UserGroupDTO;

/**
 * @author Ronyeri Marinho
 */

@Entity
public class UserGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "boardIdentifier", unique = true, nullable = false)
	private String boardIdentifier;

	@Column(nullable = false)
	private String name;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@ManyToMany
	@Cascade(CascadeType.ALL)
	@JoinTable(name = "user_usergroups", joinColumns = { @JoinColumn(name = "group_id") }, inverseJoinColumns = {
			@JoinColumn(name = "user_id") })
	private Set<User> users;

	@ManyToMany
	@JoinTable(name = "usergroup_permissions", joinColumns = { @JoinColumn(name = "group_id") }, inverseJoinColumns = {
			@JoinColumn(name = "permission_id") })
	private Set<Permission> permissions;

	@ManyToMany
	@JoinTable(name = "usergroup_milestones", joinColumns = { @JoinColumn(name = "group_id") }, inverseJoinColumns = {
			@JoinColumn(name = "milestone_id") })
	private Set<Milestone> milestones;

	@OneToOne
	@JoinColumn(name = "organization_id")
	private Organization organization;

	public UserGroup() {

	}

	public UserGroup(UserGroupDTO userGroupDTO) {
		this.setName(userGroupDTO.getName());
		this.setDeleted(false);
		this.setBoardIdentifier(userGroupDTO.getBoardIdentifier() != null ? userGroupDTO.getBoardIdentifier()
				: UUID.randomUUID().toString());

		this.users = new HashSet<User>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
}
