package br.com.board.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

/**
 * @author Ronyeri Marinho
 */

@Entity
public class Permission {

	@Id
	@Column(nullable = false, unique = true)
	private Integer permission;

	@Column(nullable = false)
	private String description;

	@ManyToMany
	@JoinTable(name = "usergroup_permissions", joinColumns = { @JoinColumn(name = "permission_id") }, inverseJoinColumns = {
			@JoinColumn(name = "group_id") })
	private Set<UserGroup> groups;

	public Integer getPermission() {
		return permission;
	}

	public void setPermission(Integer permission) {
		this.permission = permission;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<UserGroup> getGroups() {
		return groups;
	}

	public void setGroups(Set<UserGroup> groups) {
		this.groups = groups;
	}
}
