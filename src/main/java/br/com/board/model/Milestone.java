package br.com.board.model;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import br.com.board.model.enums.MilestoneTypeEnum;
import br.com.board.model.transport.MilestoneDTO;

/**
 * @author Ronyeri Marinho
 */

@Entity
public class Milestone {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "boardIdentifier", unique = true, nullable = false)
	private String boardIdentifier;

	@Column(unique = true, nullable = false)
	private Long milestoneId;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String address;

	@Column(nullable = false)
	private String token;

	@Enumerated(EnumType.STRING)
	private MilestoneTypeEnum type;

	@Column(nullable = false)
	private Long scopeId;

	@Column(nullable = false, columnDefinition = "tinyint(4) DEFAULT false")
	private Boolean closed;

	@Column(nullable = true)
	private LocalDate startDate;

	@Column(nullable = true)
	private LocalDate endDate;

	@OneToOne
	@JoinColumn(name = "organization_id")
	private Organization organization;

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;

	@OneToOne
	@JoinColumn(name = "cardset_id")
	private CardSet cardSet;

	@ManyToMany
	@JoinTable(name = "usergroup_milestones", joinColumns = {
			@JoinColumn(name = "milestone_id") }, inverseJoinColumns = { @JoinColumn(name = "group_id") })
	private Set<UserGroup> groups;

	public Milestone() {

	}

	public Milestone(MilestoneDTO milestoneDTO) {
		this.setBoardIdentifier(milestoneDTO.getBoardIdentifier() != null ? milestoneDTO.getBoardIdentifier()
				: UUID.randomUUID().toString());
		this.setMilestoneId(milestoneDTO.getMilestoneId());
		this.setName(milestoneDTO.getName());
		this.setAddress(milestoneDTO.getAddress() != null ? milestoneDTO.getAddress() : "https://gitlab.com");
		this.setToken(milestoneDTO.getToken());
		this.setType(milestoneDTO.getType());
		this.setScopeId(milestoneDTO.getScopeId());
		this.setClosed(milestoneDTO.getClosed());
		this.setStartDate(milestoneDTO.getStartDate());
		this.setEndDate(milestoneDTO.getEndDate());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public Long getMilestoneId() {
		return milestoneId;
	}

	public void setMilestoneId(Long milestoneId) {
		this.milestoneId = milestoneId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public MilestoneTypeEnum getType() {
		return type;
	}

	public void setType(MilestoneTypeEnum type) {
		this.type = type;
	}

	public Long getScopeId() {
		return scopeId;
	}

	public void setScopeId(Long scopeId) {
		this.scopeId = scopeId;
	}

	public Boolean getClosed() {
		return closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<UserGroup> getGroups() {
		return groups;
	}

	public void setGroups(Set<UserGroup> groups) {
		this.groups = groups;
	}

	public CardSet getCardSet() {
		return cardSet;
	}

	public void setCardSet(CardSet cardSet) {
		this.cardSet = cardSet;
	}
}
