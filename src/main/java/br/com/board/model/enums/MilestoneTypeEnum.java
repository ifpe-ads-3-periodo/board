package br.com.board.model.enums;

/**
 * @author Ronyeri Marinho
 */

public enum MilestoneTypeEnum {

	PROJECT("Project"), GROUP("Group");

	private String type;

	private MilestoneTypeEnum(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
}
