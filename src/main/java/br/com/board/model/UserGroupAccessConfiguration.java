package br.com.board.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Ronyeri Marinho
 */

@Entity
@Table(name = "usergroup_access_configuration")
public class UserGroupAccessConfiguration {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(nullable = false, unique = true)
	private String token;

	@Column(columnDefinition = "tinyint DEFAULT 0")
	private Boolean deleted;

	@Column(nullable = false)
	private LocalDateTime createdDate;

	@Column(nullable = false)
	private LocalDateTime expirationDate;

	@OneToOne
	@JoinColumn(name = "usergroup_id")
	private UserGroup userGroup;

	public static UserGroupAccessConfiguration build() {
		UserGroupAccessConfiguration userGroupAccessConfiguration = new UserGroupAccessConfiguration();
		userGroupAccessConfiguration.setToken(UUID.randomUUID().toString());

		LocalDateTime currentDate = LocalDateTime.now();
		userGroupAccessConfiguration.setCreatedDate(currentDate);
		userGroupAccessConfiguration.setExpirationDate(currentDate.plusMinutes(30));
		userGroupAccessConfiguration.setDeleted(false);
		return userGroupAccessConfiguration;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public UserGroup getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup) {
		this.userGroup = userGroup;
	}
}
