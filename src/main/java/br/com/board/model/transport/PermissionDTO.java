package br.com.board.model.transport;

import java.io.Serializable;

/**
 * @author Ronyeri Marinho
 */

public class PermissionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer permission;

	private String description;

	public Integer getPermission() {
		return permission;
	}

	public void setPermission(Integer permission) {
		this.permission = permission;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
