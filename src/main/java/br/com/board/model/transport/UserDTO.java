package br.com.board.model.transport;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;

import br.com.board.model.User;

/**
 * @author Ronyeri Marinho
 */

public class UserDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String boardIdentifier;

	private String username;

	private Boolean deleted;

	private String email;

	private String password;

	private String remoteAddress;

	private Set<UserGroupDTO> groups;

	public UserDTO() {

	}

	public UserDTO(User user) {
		this.setBoardIdentifier(user.getBoardIdentifier());
		this.setEmail(user.getEmail());
		this.setUsername(user.getUsername());

		if (user.getGroups() != null) {
			Set<UserGroupDTO> userGroups = user.getGroups().stream().map(UserGroupDTO::new).collect(Collectors.toSet());
			this.setGroups(userGroups);
		}
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public Set<UserGroupDTO> getGroups() {
		return groups;
	}

	public void setGroups(Set<UserGroupDTO> groups) {
		this.groups = groups;
	}
}
