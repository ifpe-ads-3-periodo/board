package br.com.board.model.transport;

import java.io.Serializable;

import br.com.board.model.Organization;

/**
 * @author Ronyeri Marinho
 */

public class OrganizationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String boardIdentifier;

	private String name;

	private Boolean deleted;

	private UserDTO user;

	public OrganizationDTO() {

	}

	public OrganizationDTO(Organization organization) {
		this.setName(organization.getName());
		this.setBoardIdentifier(organization.getBoardIdentifier());
		this.setUser(new UserDTO(organization.getUser()));
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

}
