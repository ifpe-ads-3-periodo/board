package br.com.board.model.transport;

import java.io.Serializable;
import java.util.Set;

import br.com.board.model.UserGroup;

/**
 * @author Ronyeri Marinho
 */

public class UserGroupDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String boardIdentifier;

	private String name;

	private Boolean deleted;

	private Set<UserDTO> users;

	private Set<PermissionDTO> permissions;

	private Set<MilestoneDTO> milestones;

	private OrganizationDTO organization;

	public UserGroupDTO() {

	}

	public UserGroupDTO(UserGroup userGroup) {
		this.setBoardIdentifier(userGroup.getBoardIdentifier());
		this.setName(userGroup.getName());
		this.setDeleted(userGroup.getDeleted());
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Set<UserDTO> getUsers() {
		return users;
	}

	public void setUsers(Set<UserDTO> users) {
		this.users = users;
	}

	public Set<PermissionDTO> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<PermissionDTO> permissions) {
		this.permissions = permissions;
	}

	public Set<MilestoneDTO> getMilestones() {
		return milestones;
	}

	public void setMilestones(Set<MilestoneDTO> milestones) {
		this.milestones = milestones;
	}

	public OrganizationDTO getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationDTO organization) {
		this.organization = organization;
	}
}
