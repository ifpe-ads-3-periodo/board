package br.com.board.model.transport;

import java.io.Serializable;
import java.time.LocalDateTime;

import br.com.board.model.UserGroupAccessConfiguration;

/**
 * @author Ronyeri Marinho
 */

public class UserGroupAccessConfigurationDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String token;

	private Boolean deleted;

	private LocalDateTime createdDate;

	private LocalDateTime expirationDate;

	private UserGroupDTO userGroup;

	public UserGroupAccessConfigurationDTO() {

	}

	public UserGroupAccessConfigurationDTO(UserGroupAccessConfiguration userGroupAccessConfiguration) {
		this.setToken(userGroupAccessConfiguration.getToken());
		this.setDeleted(userGroupAccessConfiguration.getDeleted());
		this.setCreatedDate(userGroupAccessConfiguration.getCreatedDate());
		this.setExpirationDate(userGroupAccessConfiguration.getExpirationDate());
		this.setUserGroup(new UserGroupDTO(userGroupAccessConfiguration.getUserGroup()));
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public UserGroupDTO getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(UserGroupDTO userGroup) {
		this.userGroup = userGroup;
	}
}
