package br.com.board.model.transport;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

import br.com.board.model.Milestone;
import br.com.board.model.enums.MilestoneTypeEnum;

/**
 * @author Ronyeri Marinho
 */

public class MilestoneDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String boardIdentifier;

	private Long milestoneId;

	private String name;

	private String address;

	private String token;

	private MilestoneTypeEnum type;

	/**
	 * Name of the project or group to which the milestone belongs
	 **/
	private String scope;

	/**
	 * Project or group identifier to which the milestone belongs
	 **/
	private Long scopeId;

	private Boolean closed;

	private LocalDate startDate;

	private LocalDate endDate;

	private OrganizationDTO organization;

	private UserDTO user;

	private CardSetDTO cardSet;

	private Set<UserGroupDTO> groups;

	public MilestoneDTO() {

	}

	public MilestoneDTO(Milestone milestone) {
		this.setBoardIdentifier(milestone.getBoardIdentifier());
		this.setAddress(milestone.getAddress());
		this.setClosed(milestone.getClosed());
		this.setEndDate(milestone.getEndDate());
		this.setMilestoneId(milestone.getMilestoneId());
		this.setName(milestone.getName());
		this.setToken(milestone.getToken());

		if (milestone.getCardSet() != null) {
			CardSetDTO cardSetDTO = new CardSetDTO(milestone.getCardSet());
			this.setCardSet(cardSetDTO);
		}

		if (milestone.getUser() != null) {
			UserDTO userDTO = new UserDTO();
			userDTO.setBoardIdentifier(milestone.getUser().getBoardIdentifier());
			userDTO.setEmail(milestone.getUser().getEmail());
			userDTO.setUsername(milestone.getUser().getUsername());
			this.setUser(userDTO);
		}

	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public Long getMilestoneId() {
		return milestoneId;
	}

	public void setMilestoneId(Long milestoneId) {
		this.milestoneId = milestoneId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public MilestoneTypeEnum getType() {
		return type;
	}

	public void setType(MilestoneTypeEnum type) {
		this.type = type;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public Long getScopeId() {
		return scopeId;
	}

	public void setScopeId(Long scopeId) {
		this.scopeId = scopeId;
	}

	public Boolean getClosed() {
		return closed;
	}

	public void setClosed(Boolean closed) {
		this.closed = closed;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public OrganizationDTO getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationDTO organization) {
		this.organization = organization;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	public CardSetDTO getCardSet() {
		return cardSet;
	}

	public void setCardSet(CardSetDTO cardSet) {
		this.cardSet = cardSet;
	}

	public Set<UserGroupDTO> getGroups() {
		return groups;
	}

	public void setGroups(Set<UserGroupDTO> groups) {
		this.groups = groups;
	}
}
