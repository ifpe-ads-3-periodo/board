package br.com.board.model.transport;

import java.io.Serializable;

import br.com.board.model.CardSet;

/**
 * @author Ronyeri Marinho
 */

public class CardSetDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	private String boardIdentifier;

	private String deck;

	private UserDTO user;

	public CardSetDTO() {

	}

	public CardSetDTO(CardSet cardSet) {
		this.setBoardIdentifier(cardSet.getBoardIdentifier());
		this.setDeck(cardSet.getDeck());
		this.setName(cardSet.getName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}
}
