package br.com.board.model.transport;

import java.io.Serializable;
import java.util.List;

import br.com.board.model.Issue;
import br.com.board.utils.StringUtils;

/**
 * @author Ronyeri Marinho
 */

public class IssueDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long issueId;

	private Long issueIid;

	private String name;

	private String description;

	private String boardIdentifier;

	private Boolean voted;

	private String point;

	private String url;

	private Long projectId;

	private List<String> labels;

	private MilestoneDTO milestone;

	public IssueDTO() {

	}

	public IssueDTO(Issue issue) {
		this.setBoardIdentifier(issue.getBoardIdentifier());
		this.setName(issue.getName());
		this.setDescription(issue.getDescription());
		this.setUrl(issue.getUrl());

		MilestoneDTO milestoneDTO = new MilestoneDTO(issue.getMilestone());
		this.setMilestone(milestoneDTO);
		this.setVoted(issue.getVoted());
		this.setLabels(StringUtils.stringToList(issue.getLabels()));
		this.setPoint(issue.getPoint());
		this.setIssueId(issue.getIssueId());
	}

	public Long getIssueId() {
		return issueId;
	}

	public void setIssueId(Long issueId) {
		this.issueId = issueId;
	}

	public Long getIssueIid() {
		return issueIid;
	}

	public void setIssueIid(Long issueIid) {
		this.issueIid = issueIid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBoardIdentifier() {
		return boardIdentifier;
	}

	public void setBoardIdentifier(String boardIdentifier) {
		this.boardIdentifier = boardIdentifier;
	}

	public Boolean getVoted() {
		return voted;
	}

	public void setVoted(Boolean voted) {
		this.voted = voted;
	}

	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public MilestoneDTO getMilestone() {
		return milestone;
	}

	public void setMilestone(MilestoneDTO milestone) {
		this.milestone = milestone;
	}

}
