package br.com.board.model.transport;

import java.io.Serializable;
import java.time.LocalDateTime;

import br.com.board.model.UserSession;

/**
 * @author Ronyeri Marinho
 */

public class UserSessionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String token;

	private Boolean deleted;

	private LocalDateTime loginDate;

	private LocalDateTime expirationDate;

	private UserDTO user;

	public UserSessionDTO() {

	}

	public UserSessionDTO(UserSession userSession) {
		this.setToken(userSession.getToken());
		this.setLoginDate(userSession.getLoginDate());
		this.setExpirationDate(userSession.getExpirationDate());
		this.setUser(new UserDTO(userSession.getUser()));
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public LocalDateTime getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(LocalDateTime loginDate) {
		this.loginDate = loginDate;
	}

	public LocalDateTime getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(LocalDateTime expirationDate) {
		this.expirationDate = expirationDate;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

}
