package br.com.board.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.board.exceptions.InvalidTokenException;
import br.com.board.model.User;
import br.com.board.model.transport.UserDTO;
import br.com.board.model.transport.UserSessionDTO;
import br.com.board.repository.UserDAO;
import br.com.board.utils.CryptographyUtils;

/**
 * @author Ronyeri Marinho
 */

@Service
public class UserService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

	private UserDAO userDAO;

	private SessionService sessionService;

	public UserService(UserDAO userDAO, SessionService sessionService) {
		this.userDAO = userDAO;
		this.sessionService = sessionService;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public User findByBoardIdentifier(String boardIdentifier) throws Exception {
		return this.userDAO.findByBoardIdentifier(boardIdentifier);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public UserDTO getUserByEmail(String email) throws Exception {
		if (email == null) {
			throw new Exception("O e-mail não foi fornecido ou é inválido");
		}
		User user = this.userDAO.findByEmail(email);
		return new UserDTO(user);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public UserDTO getUserByToken(String token) throws Exception {
		try {
			UserSessionDTO sessionByToken = this.sessionService.getSessionByToken(token);
			if (sessionByToken == null) {
				throw new InvalidTokenException("O token informado é inválido");
			}

			return sessionByToken.getUser();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new InvalidTokenException(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserDTO register(UserDTO userDTO) throws Exception {
		LOGGER.info("Iniciando cadastro do usuário...");
		try {
			if (this.checkEmailExistence(userDTO.getEmail())) {
				throw new Exception("Já existe um usuário cadastrado com o e-mail informado");
			}
			User user = new User(userDTO);
			this.userDAO.save(user);

			userDTO.setBoardIdentifier(user.getBoardIdentifier());
			return userDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public String login(UserDTO userDTO) throws Exception {
		try {
			this.validateLoginCredentials(userDTO);
			UserDTO userToLogin = this.getUserByEmail(userDTO.getEmail());
			UserSessionDTO userSession = this.sessionService.createSession(userToLogin);
			return userSession.getToken();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void logout(String token) {
		this.sessionService.logoutSession(token);
	}

	/**
	 * Checks if the email provided for the registration already exists
	 * 
	 * @author Ronyeri Marinho
	 * 
	 **/
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	private boolean checkEmailExistence(String email) {
		LOGGER.info("Verificando se o e-mail já possui cadastro...");
		User user = this.userDAO.findByEmail(email);

		if (user != null) {
			return true;
		}
		return false;
	}

	/**
	 * Validates user-provided credentials for login
	 * 
	 * @author Ronyeri Marinho
	 * 
	 **/
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	private void validateLoginCredentials(UserDTO userDTO) throws Exception {
		try {
			User userByEmail = this.getUserByEmailToLogin(userDTO.getEmail());

			if (userDTO.getPassword() == null) {
				throw new Exception("A senha não foi fornecida");
			}

			String passwordToValidate = CryptographyUtils.generateInstance()
					.createPasswordWithSalt(userDTO.getPassword(), userByEmail.getSalt());
			if (!passwordToValidate.equals(userByEmail.getPassword())) {
				throw new Exception("Senha inválida");
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Fetch the user based on the email provided to retrieve additional login
	 * information
	 * 
	 * @author Ronyeri Marinho
	 * 
	 **/
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	private User getUserByEmailToLogin(String email) throws Exception {
		if (email == null) {
			throw new Exception("O e-mail não foi fornecido ou é inválido");
		}
		User userByEmail = this.userDAO.findByEmail(email);
		if (userByEmail == null) {
			throw new Exception("Não há uma conta cadastrada com o e-mail informado");
		}
		return userByEmail;
	}

}
