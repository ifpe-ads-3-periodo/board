package br.com.board.services;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.IssueFilter;
import org.gitlab4j.api.models.Milestone;
import org.gitlab4j.api.models.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.board.exceptions.InvalidTokenException;
import br.com.board.model.enums.MilestoneTypeEnum;
import br.com.board.model.transport.IssueDTO;
import br.com.board.model.transport.MilestoneDTO;
import br.com.board.utils.DateUtils;
import br.com.board.utils.StringUtils;

/**
 * @author Ronyeri Marinho
 */

@Service
public class GitlabService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GitlabService.class);

	public GitLabApi openConnection(String address, String token) throws Exception {
		try {
			if (token == null) {
				throw new InvalidTokenException("Gitlab access token is invalid or not provided");
			}
			address = address != null ? address : "https://gitlab.com";
			return new GitLabApi(address, token);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	public void voting(MilestoneDTO milestoneDTO, IssueDTO issueDTO) throws Exception {
		try {
			GitLabApi client = this.openConnection(milestoneDTO.getAddress(), milestoneDTO.getToken());
			Issue issue = client.getIssuesApi().getIssue(issueDTO.getProjectId(), issueDTO.getIssueIid());

			if (issue == null) {
				throw new Exception("Voting will not be possible because the issue was not found in Gitlab");
			}

			String labels = StringUtils.listToString(issueDTO.getLabels());

			client.getIssuesApi().updateIssue(issueDTO.getProjectId(), issueDTO.getIssueIid(), issueDTO.getName(),
					issueDTO.getDescription(), null, null, milestoneDTO.getMilestoneId(), labels, null, null, null);

			client.close();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	public Set<IssueDTO> getIssuesByMilestone(MilestoneDTO milestoneDTO) throws Exception {
		try {
			GitLabApi client = this.openConnection(milestoneDTO.getAddress(), milestoneDTO.getToken());
			List<Issue> issues = client.getIssuesApi()
					.getIssues(new IssueFilter().withMilestone(milestoneDTO.getName()));

			if (issues == null) {
				client.close();
				return new HashSet<>();
			}

			Set<IssueDTO> issuesDTO = issues.stream().map(issue -> this.generateIssueDTO(issue, milestoneDTO))
					.collect(Collectors.toSet());

			client.close();
			return issuesDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return new HashSet<>();
		}
	}

	public MilestoneDTO getMilestoneInformations(MilestoneDTO milestoneDTO) throws Exception {
		try {
			Long scopeId = this.getMilestoneScope(milestoneDTO);
			GitLabApi client = this.openConnection(milestoneDTO.getAddress(), milestoneDTO.getToken());
			milestoneDTO.setScopeId(scopeId);
			Milestone milestone = null;

			if (milestoneDTO.getType() == MilestoneTypeEnum.PROJECT) {
				List<Milestone> milestones = client.getMilestonesApi().getMilestones(scopeId);
				if (milestones != null) {
					Optional<Milestone> optionalMilestone = milestones.stream()
							.filter(ml -> ml.getTitle().equals(milestoneDTO.getName())).findFirst();

					if (!optionalMilestone.isPresent()) {
						throw new Exception("No milestone found with the given name: " + milestoneDTO.getName());
					}
					milestone = optionalMilestone.get();

					if (milestone.getStartDate() != null) {
						milestoneDTO.setStartDate(DateUtils.dateToLocalDate(milestone.getStartDate()));
					}

					if (milestone.getDueDate() != null) {
						milestoneDTO.setEndDate(DateUtils.dateToLocalDate(milestone.getDueDate()));
					}
					milestoneDTO.setMilestoneId(milestone.getId());
				}
			} else {
				List<Milestone> milestones = client.getMilestonesApi().getGroupMilestones(scopeId);
				if (milestones != null) {
					Optional<Milestone> optionalMilestone = milestones.stream()
							.filter(ml -> ml.getTitle().equals(milestoneDTO.getName())).findFirst();

					if (!optionalMilestone.isPresent()) {
						throw new Exception("No milestone found with the given name: " + milestoneDTO.getName());
					}
					milestone = optionalMilestone.get();

					if (milestone.getStartDate() != null) {
						milestoneDTO.setStartDate(DateUtils.dateToLocalDate(milestone.getStartDate()));
					}

					if (milestone.getDueDate() != null) {
						milestoneDTO.setEndDate(DateUtils.dateToLocalDate(milestone.getDueDate()));
					}
					milestoneDTO.setMilestoneId(milestone.getId());
				}
			}
			client.close();
			return milestoneDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	private IssueDTO generateIssueDTO(Issue issue, MilestoneDTO milestonDTO) {
		IssueDTO issueDTO = new IssueDTO();
		issueDTO.setIssueId(issue.getId());
		issueDTO.setIssueIid(issue.getIid());
		issueDTO.setName(issue.getTitle());
		issueDTO.setDescription(issue.getDescription());
		issueDTO.setUrl(issue.getWebUrl());
		issueDTO.setProjectId(issue.getProjectId());
		issueDTO.setLabels(issue.getLabels());
		issueDTO.setVoted(false);

		if (milestonDTO.getCardSet() != null && milestonDTO.getCardSet().getDeck() != null) {
			List<String> points = StringUtils.stringToList(milestonDTO.getCardSet().getDeck());
			issueDTO.setPoint(this.getIssuePoints(issue.getLabels(), points));
		}

		return issueDTO;
	}

	private String getIssuePoints(List<String> labels, List<String> points) {
		for (String label : labels) {
			Optional<String> optionalPoint = points.stream().filter(point -> point.equals(label)).findFirst();
			if (optionalPoint.isPresent()) {
				return optionalPoint.get();
			}
		}
		return null;
	}

	private Long getMilestoneScope(MilestoneDTO milestoneDTO) throws Exception {
		try {
			GitLabApi client = this.openConnection(milestoneDTO.getAddress(), milestoneDTO.getToken());
			Long scope = null;

			if (milestoneDTO.getType() == MilestoneTypeEnum.PROJECT) {
				if (milestoneDTO.getScope() == null) {
					throw new Exception("The name of the project where the milestone is located must be informed");
				}

				List<Project> projects = client.getProjectApi().getMemberProjects();
				Optional<Project> optionalProject = projects.stream()
						.filter(project -> project.getName().equals(milestoneDTO.getScope())).findFirst();
				if (!optionalProject.isPresent()) {
					throw new Exception(
							"Project with the given name: " + milestoneDTO.getScope() + " not found on Gitlab");
				}
				scope = optionalProject.get().getId();

			} else if (milestoneDTO.getType() == MilestoneTypeEnum.GROUP) {
				if (milestoneDTO.getScope() == null) {
					throw new Exception("The name of the group where the milestone is located must be informed");
				}

				List<Group> groups = client.getGroupApi().getGroups();
				Optional<Group> optionalGroup = groups.stream()
						.filter(group -> group.getName().equals(milestoneDTO.getScope())).findFirst();

				if (!optionalGroup.isPresent()) {
					throw new Exception(
							"Group with the given name: " + milestoneDTO.getScope() + " not found on Gitlab");
				}
				scope = optionalGroup.get().getId();
			}
			client.close();
			return scope;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
}
