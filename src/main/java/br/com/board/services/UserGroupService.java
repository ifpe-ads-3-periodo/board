package br.com.board.services;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.board.exceptions.InvalidPermissionException;
import br.com.board.exceptions.InvalidTokenException;
import br.com.board.exceptions.OrganizationNotFoundException;
import br.com.board.exceptions.UserGroupNotFoundException;
import br.com.board.model.Organization;
import br.com.board.model.User;
import br.com.board.model.UserGroup;
import br.com.board.model.UserGroupAccessConfiguration;
import br.com.board.model.transport.OrganizationDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.model.transport.UserGroupAccessConfigurationDTO;
import br.com.board.model.transport.UserGroupDTO;
import br.com.board.repository.UserGroupAccessConfigurationDAO;
import br.com.board.repository.UserGroupDAO;

/**
 * @author Ronyeri Marinho
 */

@Service
public class UserGroupService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserGroupService.class);

	private UserGroupDAO userGroupDAO;

	private UserGroupAccessConfigurationDAO accessConfigDAO;

	private OrganizationService organizationService;

	private UserService userService;

	public UserGroupService(UserGroupDAO userGroupDAO, OrganizationService organizationService,
			UserGroupAccessConfigurationDAO userGroupAccessConfigurationDAO, UserService userService) {
		this.userGroupDAO = userGroupDAO;
		this.organizationService = organizationService;
		this.accessConfigDAO = userGroupAccessConfigurationDAO;
		this.userService = userService;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public UserGroup findByBoardIdentifier(String boardIdentifier) throws Exception {
		return this.userGroupDAO.findByBoardIdentifier(boardIdentifier);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Set<UserGroupDTO> listByUser(UserDTO userInSession) throws Exception {
		Set<UserGroupDTO> userGroupDTOs = new HashSet<>();

		List<UserGroup> userGroups = this.userGroupDAO.list();
		if (userGroups == null) {
			return userGroupDTOs;
		}

		for (UserGroup userGroup : userGroups) {
			if (userGroup.getOrganization().getUser().getBoardIdentifier().equals(userInSession.getBoardIdentifier())) {
				UserGroupDTO userGroupDTO = new UserGroupDTO(userGroup);
				userGroupDTOs.add(userGroupDTO);
			}
			if (userGroup.getUsers() != null && !userGroup.getUsers().isEmpty()) {
				Optional<User> optionalUser = userGroup.getUsers().stream()
						.filter(user -> user.getBoardIdentifier().equals(userInSession.getBoardIdentifier()))
						.findFirst();
				if (optionalUser.isPresent()) {
					UserGroupDTO userGroupDTO = new UserGroupDTO(userGroup);
					userGroupDTOs.add(userGroupDTO);
				}
			}
		}

		return userGroupDTOs;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Set<UserDTO> listUsersByUserGroup(String userGroupUUID, UserDTO userInSession) throws Exception {
		try {
			UserGroup userGroup = this.userGroupDAO.findByBoardIdentifier(userGroupUUID);

			if (userGroup == null) {
				throw new UserGroupNotFoundException("No user group was found with this identifier: " + userGroupUUID);
			}

			if (!userGroup.getOrganization().getUser().getBoardIdentifier()
					.equals(userInSession.getBoardIdentifier())) {
				throw new InvalidPermissionException("Only the organization owner can create user groups");
			}

			if (userGroup.getUsers() == null || userGroup.getUsers().isEmpty()) {
				return new HashSet<>();
			}

			return userGroup.getUsers().stream().map(User::generateTransportWithoutGroups).collect(Collectors.toSet());
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new InvalidPermissionException(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserGroupDTO create(UserGroupDTO userGroupDTO, String organizationUUID, UserDTO userInSession)
			throws Exception {
		try {
			Organization organization = this.organizationService.findByBoardIdentifier(organizationUUID);
			if (organization == null) {
				throw new OrganizationNotFoundException(
						"No organization was found with this identifier: " + organizationUUID);
			}

			if (!organization.getUser().getBoardIdentifier().equals(userInSession.getBoardIdentifier())) {
				throw new InvalidPermissionException("Only the organization owner can create user groups");
			}

			UserGroup userGroup = new UserGroup(userGroupDTO);
			userGroup.setOrganization(organization);
			this.userGroupDAO.save(userGroup);

			userGroupDTO.setBoardIdentifier(userGroup.getBoardIdentifier());
			userGroupDTO.setOrganization(new OrganizationDTO(organization));
			return userGroupDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(String userGroupUUID, UserDTO userInSession) throws Exception {
		try {
			UserGroup userGroup = this.userGroupDAO.findByBoardIdentifier(userGroupUUID);

			if (userGroup == null) {
				throw new UserGroupNotFoundException("No user group was found with this identifier: " + userGroupUUID);
			}

			if (!userGroup.getOrganization().getUser().getBoardIdentifier()
					.equals(userInSession.getBoardIdentifier())) {
				throw new InvalidPermissionException("Only the user group owner can delete it");
			}

			userGroup.setDeleted(true);
			this.userGroupDAO.save(userGroup);
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new InvalidPermissionException(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserGroupAccessConfigurationDTO createAccessToken(String userGroupUUID, UserDTO userInSession)
			throws Exception {
		try {
			UserGroup userGroup = this.userGroupDAO.findByBoardIdentifier(userGroupUUID);
			if (userGroup == null) {
				throw new UserGroupNotFoundException("No user group was found with this identifier: " + userGroupUUID);
			}

			if (!userGroup.getOrganization().getUser().getBoardIdentifier()
					.equals(userInSession.getBoardIdentifier())) {
				throw new InvalidPermissionException("Only the owner can generate access tokens for the user group");
			}

			this.deletePreviousAccessTokens(userGroupUUID);

			UserGroupAccessConfiguration userGroupAccessConfiguration = UserGroupAccessConfiguration.build();
			userGroupAccessConfiguration.setUserGroup(userGroup);

			this.accessConfigDAO.save(userGroupAccessConfiguration);

			return new UserGroupAccessConfigurationDTO(userGroupAccessConfiguration);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void deleteAccessToken(String userGroupUUID, String token, UserDTO userInSession) throws Exception {
		try {
			UserGroup userGroup = this.userGroupDAO.findByBoardIdentifier(userGroupUUID);
			if (userGroup == null) {
				throw new UserGroupNotFoundException("No user group was found with this identifier: " + userGroupUUID);
			}

			if (!userGroup.getOrganization().getUser().getBoardIdentifier()
					.equals(userInSession.getBoardIdentifier())) {
				throw new InvalidPermissionException("Only the owner can delete access tokens for the user group");
			}

			UserGroupAccessConfiguration accessConfig = this.accessConfigDAO.findByTokenAndUserGroup(userGroupUUID,
					token);
			if (accessConfig == null) {
				throw new InvalidTokenException(
						"The group access token provided has expired, is invalid or does not exist");
			}

			this.accessConfigDAO.delete(accessConfig);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addUser(UserGroupAccessConfigurationDTO accessConfigDTO, String userGroupUUID, UserDTO userInSession)
			throws Exception {
		try {
			UserGroup userGroup = this.userGroupDAO.findByBoardIdentifier(userGroupUUID);
			if (userGroup == null) {
				throw new UserGroupNotFoundException("No user group was found with this identifier: " + userGroupUUID);
			}

			UserGroupAccessConfiguration accessConfig = this.accessConfigDAO.findByTokenAndUserGroup(userGroupUUID,
					accessConfigDTO.getToken());
			if (accessConfig == null) {
				throw new InvalidTokenException(
						"The group access token provided has expired, is invalid or does not exist");
			}

			Optional<User> userOptional = userGroup.getUsers().stream()
					.filter(user -> user.getBoardIdentifier().equals(userInSession.getBoardIdentifier())).findFirst();
			if (userOptional.isPresent()) {
				throw new Exception("The user is already in the group");
			}

			User user = this.userService.findByBoardIdentifier(userInSession.getBoardIdentifier());
			userGroup.getUsers().add(user);
			this.userGroupDAO.save(userGroup);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void clearExpiredTokens() {
		List<UserGroupAccessConfiguration> activeTokens = this.accessConfigDAO.listActiveTokens();

		if (activeTokens != null && !activeTokens.isEmpty()) {
			activeTokens.stream().forEach(token -> this.isAccessTokenExpired(token));
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Boolean isAccessTokenExpired(UserGroupAccessConfiguration userGroupAccessConfiguration) {
		if (LocalDateTime.now().isBefore(userGroupAccessConfiguration.getExpirationDate())) {
			return false;
		}
		this.accessConfigDAO.delete(userGroupAccessConfiguration);
		return true;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private void deletePreviousAccessTokens(String userGroupUUID) throws Exception {
		try {
			LOGGER.info("Removing previous tokens from the group");
			List<UserGroupAccessConfiguration> activeTokens = this.accessConfigDAO
					.listActiveTokensByUserGroup(userGroupUUID);

			if (activeTokens != null && !activeTokens.isEmpty()) {
				activeTokens.forEach(activeToken -> this.accessConfigDAO.delete(activeToken));
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
}
