package br.com.board.services;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.board.exceptions.InvalidTokenException;
import br.com.board.model.User;
import br.com.board.model.UserSession;
import br.com.board.model.transport.UserDTO;
import br.com.board.model.transport.UserSessionDTO;
import br.com.board.repository.UserDAO;
import br.com.board.repository.UserSessionDAO;

/**
 * @author Ronyeri Marinho
 */

@Service
public class SessionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SessionService.class);

	private UserDAO userDAO;

	private UserSessionDAO userSessionDAO;

	public SessionService(UserDAO userDAO, UserSessionDAO userSessionDAO) {
		this.userDAO = userDAO;
		this.userSessionDAO = userSessionDAO;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public UserSessionDTO getSessionByToken(String token) throws InvalidTokenException {
		UserSession userSession = this.userSessionDAO.findByToken(token);
		if (userSession == null) {
			throw new InvalidTokenException("Session expired, you need to login again to generate a valid token");
		}

		if (this.isSessionExpired(userSession)) {
			throw new InvalidTokenException("Session expired, you need to login again to generate a valid token");
		}
		return new UserSessionDTO(userSession);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public UserSessionDTO createSession(UserDTO userDTO) throws Exception {
		UserSession sessionByUserUUID = this.getSessionByUserUUID(userDTO.getBoardIdentifier());
		if (sessionByUserUUID != null) {
			if (!this.isSessionExpired(sessionByUserUUID)) {
				LOGGER.info("The user has an active session, it will be returned");
				return new UserSessionDTO(sessionByUserUUID);
			}
		}
		LOGGER.info("The user does not have an active session, creating...");
		User user = this.userDAO.findByBoardIdentifier(userDTO.getBoardIdentifier());

		UserSession userSession = UserSession.build();
		userSession.setUser(user);
		UserSessionDTO userSessionObject = this.createSession(userSession);
		return userSessionObject;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void logoutSession(String token) {
		UserSession session = this.userSessionDAO.findByToken(token);
		if (session != null) {
			session.setLogoutDate(LocalDateTime.now());
			session.setDeleted(true);
			this.userSessionDAO.save(session);
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public Boolean isSessionExpired(UserSession userSession) {
		if (LocalDateTime.now().isBefore(userSession.getExpirationDate())) {
			return false;
		}
		this.userSessionDAO.delete(userSession.getToken());
		return true;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void clearExpiredSessions() {
		List<UserSession> sessions = this.userSessionDAO.listActiveSessions();

		if (sessions != null && !sessions.isEmpty()) {
			sessions.stream().forEach(session -> this.isSessionExpired(session));
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	private UserSessionDTO createSession(UserSession userSession) {
		return new UserSessionDTO(this.userSessionDAO.save(userSession));
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	private UserSession getSessionByUserUUID(String userUUID) {
		return this.userSessionDAO.findByUserUUID(userUUID);

	}

}
