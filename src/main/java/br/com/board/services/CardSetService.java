package br.com.board.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.board.model.CardSet;
import br.com.board.model.User;
import br.com.board.model.transport.CardSetDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.repository.CardSetDAO;
import br.com.board.utils.StringUtils;

/**
 * @author Ronyeri Marinho
 */

@Service
public class CardSetService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CardSetService.class);

	private CardSetDAO cardSetDAO;

	private UserService userService;

	public CardSetService(CardSetDAO cardSetDAO, UserService userService) {
		this.cardSetDAO = cardSetDAO;
		this.userService = userService;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public CardSet findByBoardIdentifier(String boardIdentifier) throws Exception {
		CardSet cardSet = this.cardSetDAO.findByBoardIdentifier(boardIdentifier);
		if (cardSet == null) {
			throw new Exception("Card set with the id: " + boardIdentifier + " was not found:");
		}
		return cardSet;
	}

	public List<String> getDeck(String boardIdentifier, UserDTO userInSession) throws Exception {
		CardSet cardSet = this.findByBoardIdentifier(boardIdentifier);
		return StringUtils.stringToList(cardSet.getDeck());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public CardSetDTO createCardSet(CardSetDTO cardSetDTO, UserDTO userInSession) throws Exception {
		try {
			if (cardSetDTO.getDeck() == null) {
				throw new Exception("To create a set of cards, the cards must be chosen");
			}
			CardSet cardSet = new CardSet(cardSetDTO);

			User user = this.userService.findByBoardIdentifier(userInSession.getBoardIdentifier());
			cardSet.setUser(user);
			cardSetDAO.save(cardSet);

			cardSetDTO.setBoardIdentifier(cardSet.getBoardIdentifier());
			return cardSetDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
}
