package br.com.board.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.board.model.CardSet;
import br.com.board.model.Milestone;
import br.com.board.model.Organization;
import br.com.board.model.User;
import br.com.board.model.UserGroup;
import br.com.board.model.transport.MilestoneDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.model.transport.UserGroupDTO;
import br.com.board.repository.MilestoneDAO;

/**
 * @author Ronyeri Marinho
 */

@Service
public class MilestoneService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MilestoneService.class);

	private GitlabService gitlabService;

	private OrganizationService organizationService;

	private UserService userService;

	private UserGroupService userGroupService;

	private MilestoneDAO milestoneDAO;

	private CardSetService cardSetService;

	public MilestoneService(GitlabService gitlabService, OrganizationService organizationService,
			UserService userService, UserGroupService userGroupService, MilestoneDAO milestoneDAO,
			CardSetService cardSetService) {
		this.gitlabService = gitlabService;
		this.organizationService = organizationService;
		this.userService = userService;
		this.userGroupService = userGroupService;
		this.milestoneDAO = milestoneDAO;
		this.cardSetService = cardSetService;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Milestone findByBoardIdentifier(String id) throws Exception {
		return this.milestoneDAO.findByBoardIdentifier(id);
	}

	public List<Milestone> listMilestones() {
		List<Milestone> milestones = this.milestoneDAO.listMilestones();
		return milestones != null ? milestones : new ArrayList<>();
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public MilestoneDTO create(MilestoneDTO milestoneDTO, UserDTO userInSession) throws Exception {
		try {
			MilestoneDTO milestoneInfo = this.gitlabService.getMilestoneInformations(milestoneDTO);
			Milestone milestone = new Milestone(milestoneInfo);

			Organization organization = this.organizationService
					.findByBoardIdentifier(milestoneDTO.getOrganization().getBoardIdentifier());
			milestone.setOrganization(organization);

			Set<UserGroup> userGroups = new HashSet<>();
			if (milestoneDTO.getGroups() != null) {
				for (UserGroupDTO userGroupDTO : milestoneDTO.getGroups()) {
					UserGroup userGroup = this.userGroupService
							.findByBoardIdentifier(userGroupDTO.getBoardIdentifier());
					userGroups.add(userGroup);
				}
				milestone.setGroups(userGroups);
			}

			User user = this.userService.findByBoardIdentifier(userInSession.getBoardIdentifier());
			milestone.setUser(user);

			if (milestoneDTO.getCardSet() != null && milestoneDTO.getCardSet().getBoardIdentifier() != null) {
				CardSet cardSet = this.cardSetService
						.findByBoardIdentifier(milestoneDTO.getCardSet().getBoardIdentifier());
				milestone.setCardSet(cardSet);
			}

			milestoneDAO.save(milestone);
			milestoneInfo.setBoardIdentifier(milestone.getBoardIdentifier());
			return milestoneInfo;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

}
