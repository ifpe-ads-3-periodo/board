package br.com.board.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.board.exceptions.InvalidPermissionException;
import br.com.board.exceptions.OrganizationNotFoundException;
import br.com.board.model.Organization;
import br.com.board.model.User;
import br.com.board.model.transport.OrganizationDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.repository.OrganizationDAO;

/**
 * @author Ronyeri Marinho
 */

@Service
public class OrganizationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrganizationService.class);

	private UserService userService;

	private OrganizationDAO organizationDAO;

	public OrganizationService(UserService userService, OrganizationDAO organizationDAO) {
		this.userService = userService;
		this.organizationDAO = organizationDAO;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public Organization findByBoardIdentifier(String organizationUUID) {
		return this.organizationDAO.findByBoardIdentifierAndDeletedFalse(organizationUUID);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<OrganizationDTO> list(UserDTO userInSession) throws Exception {
		User user = this.userService.findByBoardIdentifier(userInSession.getBoardIdentifier());
		List<Organization> organizations = this.organizationDAO.findByUserAndDeletedFalse(user, Sort.by("name"));
		if (organizations == null) {
			return new ArrayList<>();
		}
		return organizations.stream().map(OrganizationDTO::new).collect(Collectors.toList());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public OrganizationDTO create(OrganizationDTO organizationDTO, UserDTO userInSession) throws Exception {
		try {
			User user = this.userService.findByBoardIdentifier(userInSession.getBoardIdentifier());
			Organization organization = new Organization(organizationDTO, user);
			this.organizationDAO.save(organization);

			organizationDTO.setBoardIdentifier(organization.getBoardIdentifier());
			organizationDTO.setUser(userInSession);
			return organizationDTO;
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void delete(String organizationUUID, UserDTO userInSession) throws Exception {
		try {
			Organization organization = this.organizationDAO.findByBoardIdentifierAndDeletedFalse(organizationUUID);

			if (organization == null) {
				throw new OrganizationNotFoundException(
						"No organization was found with this identifier: " + organizationUUID);
			}

			if (!organization.getUser().getBoardIdentifier().equals(userInSession.getBoardIdentifier())) {
				throw new InvalidPermissionException("Only the organization owner can delete it");
			}

			organization.setDeleted(true);
			this.organizationDAO.save(organization);
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new InvalidPermissionException(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
}
