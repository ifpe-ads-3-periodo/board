package br.com.board.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.board.exceptions.InvalidPermissionException;
import br.com.board.model.Issue;
import br.com.board.model.Milestone;
import br.com.board.model.transport.IssueDTO;
import br.com.board.model.transport.MilestoneDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.repository.IssueDAO;
import br.com.board.utils.StringUtils;

/**
 * @author Ronyeri Marinho
 */

@Service
public class IssueService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssueService.class);

	private GitlabService gitlabService;

	private MilestoneService milestoneService;

	private IssueDAO issueDAO;

	public IssueService(GitlabService gitlabService, MilestoneService milestoneService, IssueDAO issueDAO) {
		this.gitlabService = gitlabService;
		this.milestoneService = milestoneService;
		this.issueDAO = issueDAO;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<IssueDTO> listIssuesToVoting(String milestoneId, UserDTO userInSession) {
		List<Issue> issues = this.issueDAO.listByMilestoneToVoting(milestoneId, userInSession.getBoardIdentifier());
		return issues.stream().map(IssueDTO::new).collect(Collectors.toList());
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<Issue> listIssuesByMilestone(String milestoneId) {
		return this.issueDAO.listByMilestone(milestoneId);
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void vote(IssueDTO issueDTO, UserDTO userInSession) throws Exception {
		try {
			if (issueDTO.getMilestone() == null || issueDTO.getMilestone().getBoardIdentifier() == null) {
				throw new Exception("Unable to vote the issue because the milestone was not found");
			}

			Milestone milestone = this.milestoneService
					.findByBoardIdentifier(issueDTO.getMilestone().getBoardIdentifier());
			MilestoneDTO milestoneDTO = new MilestoneDTO(milestone);

			if (!userInSession.getBoardIdentifier().equals(milestoneDTO.getUser().getBoardIdentifier())) {
				throw new InvalidPermissionException("The user: " + userInSession.getUsername()
						+ " does not have permission to perform the operation");
			}

			Issue issue = this.issueDAO.findByBoardIdentifier(issueDTO.getBoardIdentifier());
			if (issue == null) {
				throw new Exception("Issue to voting not found");
			}

			String deck = milestoneDTO.getCardSet().getDeck();
			List<String> labels = this.updateReevaluateIssueLabels(issueDTO.getLabels(), issueDTO.getPoint(), deck);
			issueDTO.setLabels(labels);

			this.gitlabService.voting(milestoneDTO, issueDTO);
			issue.setLabels(StringUtils.listToString(issueDTO.getLabels()));
			issue.setPoint(issueDTO.getPoint());
			issue.setVoted(true);
			this.issueDAO.save(issue);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	private List<String> updateReevaluateIssueLabels(List<String> labels, String newPoint, String deck) {
		labels.removeIf(label -> label.equalsIgnoreCase("REAVALIAR PONTOS"));

		List<String> points = StringUtils.stringToList(deck);
		points.forEach(point -> {
			labels.removeIf(label -> label.equalsIgnoreCase(point) && !label.equalsIgnoreCase(newPoint));
		});

		return labels;
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void syncIssues(Milestone milestone) {
		try {
			MilestoneDTO milestoneDTO = new MilestoneDTO(milestone);
			Set<IssueDTO> retrievedIssues = this.gitlabService.getIssuesByMilestone(milestoneDTO);
			List<Issue> currentIssues = this.listIssuesByMilestone(milestone.getBoardIdentifier());

			retrievedIssues.forEach(retrievedIssue -> {

				Optional<Issue> optionalIssue = currentIssues.stream()
						.filter(currentIssue -> currentIssue.getIssueId().equals(retrievedIssue.getIssueId()))
						.findFirst();

				if (optionalIssue.isPresent()) {
					Issue issue = optionalIssue.get();
					Issue issueToUpdate = this.updateIssue(issue, retrievedIssue);
					List<String> points = StringUtils.stringToList(milestone.getCardSet().getDeck());

					issueToUpdate.setVoted(this.isVoted(points, retrievedIssue.getLabels()));
					this.issueDAO.save(issueToUpdate);

					currentIssues
							.removeIf(currentIssue -> currentIssue.getIssueId().equals(retrievedIssue.getIssueId()));
				} else {
					Issue issueToCreate = new Issue(retrievedIssue);

					List<String> points = StringUtils.stringToList(milestone.getCardSet().getDeck());

					issueToCreate.setVoted(this.isVoted(points, retrievedIssue.getLabels()));
					issueToCreate.setMilestone(milestone);
					this.issueDAO.save(issueToCreate);
				}
			});

			currentIssues.forEach(currentIssue -> {
				this.issueDAO.delete(currentIssue);
			});
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
	}

	private Issue updateIssue(Issue issue, IssueDTO issueDTO) {
		issue.setDescription(issueDTO.getDescription());
		issue.setName(issueDTO.getName());
		issue.setLabels(StringUtils.listToString(issueDTO.getLabels()));
		issue.setVoted(issueDTO.getVoted());
		issue.setProjectId(issueDTO.getProjectId());
		issue.setIssueIid(issueDTO.getIssueIid());
		issue.setPoint(issueDTO.getPoint());
		return issue;
	}

	private Boolean isVoted(List<String> points, List<String> labels) {
		for (String label : labels) {
			Optional<String> optionalPoint = points.stream().filter(point -> point.equals(label)).findFirst();
			if (optionalPoint.isPresent()) {
				return true;
			}
		}
		return false;
	}
}
