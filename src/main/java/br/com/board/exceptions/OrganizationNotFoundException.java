package br.com.board.exceptions;

/**
 * @author Ronyeri Marinho
 */

public class OrganizationNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public OrganizationNotFoundException() {

	}

	public OrganizationNotFoundException(String message) {
		super(message);
	}

	public OrganizationNotFoundException(String message, Throwable e) {
		super(message, e);
	}
}
