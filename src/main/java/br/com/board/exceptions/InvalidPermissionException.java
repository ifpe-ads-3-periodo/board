package br.com.board.exceptions;

/**
 * @author Ronyeri Marinho
 */

public class InvalidPermissionException extends Exception {

	private static final long serialVersionUID = 1L;

	public InvalidPermissionException() {

	}

	public InvalidPermissionException(String message) {
		super(message);
	}

	public InvalidPermissionException(String message, Throwable e) {
		super(message, e);
	}
}
