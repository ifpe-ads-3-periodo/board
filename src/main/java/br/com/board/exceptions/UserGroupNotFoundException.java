package br.com.board.exceptions;

/**
 * @author Ronyeri Marinho
 */

public class UserGroupNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserGroupNotFoundException() {

	}

	public UserGroupNotFoundException(String message) {
		super(message);
	}

	public UserGroupNotFoundException(String message, Throwable e) {
		super(message, e);
	}
}
