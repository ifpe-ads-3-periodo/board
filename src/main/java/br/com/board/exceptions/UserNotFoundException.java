package br.com.board.exceptions;

/**
 * @author Ronyeri Marinho
 */

public class UserNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public UserNotFoundException() {

	}

	public UserNotFoundException(String message) {
		super(message);
	}

	public UserNotFoundException(String message, Throwable e) {
		super(message, e);
	}
}
