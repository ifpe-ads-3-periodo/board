package br.com.board.controllers;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.board.exceptions.InvalidPermissionException;
import br.com.board.exceptions.InvalidTokenException;
import br.com.board.model.transport.UserDTO;
import br.com.board.model.transport.UserGroupAccessConfigurationDTO;
import br.com.board.model.transport.UserGroupDTO;
import br.com.board.services.UserGroupService;
import br.com.board.services.UserService;

/**
 * @author Ronyeri Marinho
 */

@RestController
@RequestMapping("/user-group")
public class UserGroupController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserGroupController.class);

	private UserGroupService userGroupService;

	private UserService userService;

	public UserGroupController(UserGroupService userGroupService, UserService userService) {
		this.userGroupService = userGroupService;
		this.userService = userService;
	}

	@GetMapping
	public ResponseEntity<Set<UserGroupDTO>> list(@RequestHeader("token") final String token) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			Set<UserGroupDTO> groups = this.userGroupService.listByUser(userInSession);
			if (groups.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			return ResponseEntity.ok(groups);
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@GetMapping("/{id}/users")
	public ResponseEntity<Set<UserDTO>> listUsersByUserGroup(@RequestHeader("token") final String token,
			@PathVariable("id") String userGroupUUID) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			Set<UserDTO> users = this.userGroupService.listUsersByUserGroup(userGroupUUID, userInSession);
			if (users.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			return ResponseEntity.ok(users);
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping
	public ResponseEntity<UserGroupDTO> create(@RequestHeader("token") final String token,
			@RequestHeader("organization") final String organizationUUID, @RequestBody UserGroupDTO userGroupDTO)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			UserGroupDTO response = this.userGroupService.create(userGroupDTO, organizationUUID, userInSession);
			return ResponseEntity.ok(response);
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@RequestHeader("token") final String token, @PathVariable("id") String id)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			this.userGroupService.delete(id, userInSession);
			return ResponseEntity.ok().build();
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping("/{id}/access-token")
	public ResponseEntity<UserGroupAccessConfigurationDTO> createAccessToken(@RequestHeader("token") final String token,
			@PathVariable("id") String userGroupUUID) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			UserGroupAccessConfigurationDTO response = this.userGroupService.createAccessToken(userGroupUUID,
					userInSession);
			return ResponseEntity.ok(response);
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@DeleteMapping("/{id}/access-token/{tokenUUID}")
	public ResponseEntity<Void> deleteAccessToken(@RequestHeader("token") final String token,
			@PathVariable("id") String userGroupUUID, @PathVariable("tokenUUID") String id) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			this.userGroupService.deleteAccessToken(userGroupUUID, id, userInSession);
			return ResponseEntity.ok().build();
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PutMapping("/{id}/user")
	public ResponseEntity<Void> addUser(@RequestHeader("token") final String token,
			@PathVariable("id") String userGroupUUID, @RequestBody UserGroupAccessConfigurationDTO accessConfigDTO)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			this.userGroupService.addUser(accessConfigDTO, userGroupUUID, userInSession);
			return ResponseEntity.ok().build();
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
}
