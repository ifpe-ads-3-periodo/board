package br.com.board.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.board.exceptions.InvalidPermissionException;
import br.com.board.exceptions.InvalidTokenException;
import br.com.board.model.transport.IssueDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.services.IssueService;
import br.com.board.services.UserService;

/**
 * @author Ronyeri Marinho
 */

@RestController
@RequestMapping("/issue")
public class IssueController {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssueController.class);

	private IssueService issueService;

	private UserService userService;

	public IssueController(IssueService issueService, UserService userService) {
		this.issueService = issueService;
		this.userService = userService;
	}

	@GetMapping("/{milestoneId}/voting")
	public ResponseEntity<List<IssueDTO>> listToVoting(@RequestHeader("token") final String token,
			@PathVariable("milestoneId") String milestoneId) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			List<IssueDTO> issues = this.issueService.listIssuesToVoting(milestoneId, userInSession);
			if (issues.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			return ResponseEntity.ok(issues);
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping("/{id}/vote")
	public ResponseEntity<Void> vote(@RequestHeader("token") final String token, @RequestBody IssueDTO issueDTO)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			this.issueService.vote(issueDTO, userInSession);
			return ResponseEntity.ok().build();

		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
}
