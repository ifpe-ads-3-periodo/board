package br.com.board.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.board.exceptions.InvalidPermissionException;
import br.com.board.exceptions.InvalidTokenException;
import br.com.board.model.transport.CardSetDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.services.CardSetService;
import br.com.board.services.UserService;

/**
 * @author Ronyeri Marinho
 */

@RestController
@RequestMapping("/card-set")
public class CardSetController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CardSetController.class);

	private UserService userService;
	private CardSetService cardSetService;

	public CardSetController(UserService userService, CardSetService cardSetService) {
		this.userService = userService;
		this.cardSetService = cardSetService;
	}

	@GetMapping("/{id}/deck")
	public ResponseEntity<List<String>> listDeckPoints(@RequestHeader("token") final String token,
			@PathVariable("id") String id) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			List<String> response = this.cardSetService.getDeck(id, userInSession);
			if (response.isEmpty()) {
				return ResponseEntity.noContent().build();
			}
			return ResponseEntity.ok(response);
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@PostMapping
	public ResponseEntity<CardSetDTO> createCardSet(@RequestHeader("token") final String token,
			@RequestBody CardSetDTO cardSetDTO) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			CardSetDTO response = this.cardSetService.createCardSet(cardSetDTO, userInSession);
			return ResponseEntity.ok(response);
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
}
