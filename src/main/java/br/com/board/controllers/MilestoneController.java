package br.com.board.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.board.exceptions.InvalidTokenException;
import br.com.board.model.transport.MilestoneDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.services.MilestoneService;
import br.com.board.services.UserService;

/**
 * @author Ronyeri Marinho
 */

@RestController
@RequestMapping("/milestone")
public class MilestoneController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MilestoneController.class);

	private UserService userService;

	private MilestoneService milestoneService;

	public MilestoneController(UserService userService, MilestoneService milestoneService) {
		this.userService = userService;
		this.milestoneService = milestoneService;
	}

	@PostMapping
	public ResponseEntity<MilestoneDTO> create(@RequestHeader("token") final String token,
			@RequestBody MilestoneDTO milestoneDTO) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);
			MilestoneDTO response = this.milestoneService.create(milestoneDTO, userInSession);
			return ResponseEntity.ok(response);
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}
}
