package br.com.board.controllers.schedulers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.board.services.UserGroupService;

/**
 * @author Ronyeri Marinho
 */

@Component
@EnableAsync
public class CheckUserGroupTokensScheduler {

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckUserSessionScheduler.class);

	private UserGroupService userGroupService;

	public CheckUserGroupTokensScheduler(UserGroupService userGroupService) {
		this.userGroupService = userGroupService;
	}

	@Async
	@Scheduled(fixedDelay = 180000)
	public void clearUserGroupTokensExpired() {
		LOGGER.info("Fetching and removing expired user group tokens...");
		this.userGroupService.clearExpiredTokens();
	}
}
