package br.com.board.controllers.schedulers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.board.model.Milestone;
import br.com.board.services.IssueService;
import br.com.board.services.MilestoneService;

/**
 * @author Ronyeri Marinho
 */

@Component
@EnableAsync
public class SyncMilestoneIssuesScheduler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SyncMilestoneIssuesScheduler.class);

	private MilestoneService milestoneService;

	private IssueService issueService;

	public SyncMilestoneIssuesScheduler(MilestoneService milestoneService, IssueService issueService) {
		this.milestoneService = milestoneService;
		this.issueService = issueService;
	}

	@Async
	@Scheduled(fixedDelay = 180000)
	public void syncMilestoneIssues() {
		LOGGER.info("Syncing milestone issues...");
		List<Milestone> milestones = this.milestoneService.listMilestones();
		milestones.forEach(milestone -> {
			this.issueService.syncIssues(milestone);
		});
	}
}
