package br.com.board.controllers.schedulers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.board.services.SessionService;

/**
 * @author Ronyeri Marinho
 */

@Component
@EnableAsync
public class CheckUserSessionScheduler {

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckUserSessionScheduler.class);

	private SessionService sessionService;

	public CheckUserSessionScheduler(SessionService sessionService) {
		this.sessionService = sessionService;
	}

	@Async
	@Scheduled(fixedDelay = 180000)
	public void clearUserSessionExpired() {
		LOGGER.info("Fetching and terminating expired sessions...");
		this.sessionService.clearExpiredSessions();
	}
}
