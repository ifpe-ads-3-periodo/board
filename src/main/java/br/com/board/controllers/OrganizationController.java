package br.com.board.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.board.exceptions.InvalidPermissionException;
import br.com.board.exceptions.InvalidTokenException;
import br.com.board.model.transport.OrganizationDTO;
import br.com.board.model.transport.UserDTO;
import br.com.board.services.OrganizationService;
import br.com.board.services.UserService;

/**
 * @author Ronyeri Marinho
 */

@RestController
@RequestMapping("/organization")
public class OrganizationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrganizationController.class);

	private UserService userService;

	private OrganizationService organizationService;

	public OrganizationController(UserService userService, OrganizationService organizationService) {
		this.userService = userService;
		this.organizationService = organizationService;
	}

	@GetMapping
	public ResponseEntity<List<OrganizationDTO>> list(@RequestHeader("token") final String token) throws Exception {
		UserDTO userInSession = this.userService.getUserByToken(token);
		List<OrganizationDTO> response = this.organizationService.list(userInSession);
		if (response.isEmpty()) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(response);
	}

	@PostMapping
	public ResponseEntity<OrganizationDTO> create(@RequestHeader("token") final String token,
			@RequestBody OrganizationDTO organizationDTO) throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			OrganizationDTO response = this.organizationService.create(organizationDTO, userInSession);
			return ResponseEntity.ok(response);
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@RequestHeader("token") final String token, @PathVariable("id") String id)
			throws Exception {
		try {
			UserDTO userInSession = this.userService.getUserByToken(token);

			this.organizationService.delete(id, userInSession);
			return ResponseEntity.ok().build();
		} catch (InvalidPermissionException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (InvalidTokenException e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

}
